OPTIONS= -Wall

all: afn.o ensemble.o pile.o soutenance

soutenance: ensemble.o pile.o afn.o main_soutenance.o test_soutenance.o
	gcc $(OPTIONS) ensemble.o pile.o afn.o main_soutenance.o test_soutenance.o -o soutenance

afn.o: ensemble.h pile.h afn.c
	gcc $(OPTIONS) -c afn.c 

ensemble.o: ensemble.c
	gcc $(OPTIONS) -c ensemble.c

pile.o: pile.c
	gcc $(OPTIONS) -c pile.c


clean:
	rm -f *.o *~ soutenance
