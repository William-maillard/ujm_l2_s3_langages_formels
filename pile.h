#define TMAX 100
typedef int pile[TMAX]; /*L'indice 0 stockera le nombre d�l�ment de notre liste et le dernier element insere se trouvera a l'indice i= pile[0]*/

/*declaration des 5 fonctions sur les piles*/

/*initialise la pile p avant sa premiere utilisation*/
int PILE_initialiser(pile p);

/*empile un element sur la pile p*/
int PILE_empiler(int elem, pile p);

/*retourne 1 si la pile p est vide 0 sinon*/
int PILE_estvide(pile p);

/*retourne l'element sommet de la pile p et enleve cet element de la pile*/
int PILE_depiler(pile p);

/*affiche la pile p*/
int PILE_afficher(pile p);
