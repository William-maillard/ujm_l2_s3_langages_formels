#include<stdio.h>
#include<stdlib.h>
#define TMAX 100

typedef int ensemble[TMAX];/*on choisi la case d'indice 0 pour stocker le cardinal, et le nombre i est present si la case d'indice i vaut 1, 0 sinon*/

/*initialise l'ensemble e*/
int ENS_initialiser(ensemble e){
  int i;
  for(i=0; i<TMAX; i++){
    e[i]= 0;
  }
  return 1;
}

/*ajoute l’élément elem a l’ensemble e*/
int ENS_ajouter(int elem, ensemble e){
  if(elem>TMAX-1){
    printf("Vous tentez d'inserer un element supperieur a %d\n", TMAX);
    return 0;
  }
  if(e[elem]==0){/*on rajoute l'element que si il est absent*/
    e[elem]=1;
    e[0]++;
  }
  return 1;
}

/* retourne 1 si l’élément elem appartient à l’ensemble e; 0 sinon */
int ENS_appartient(int elem, ensemble e){
  return e[elem];
}


/*retourne 1 si l’ensemble est vide, 0 sinon*/
int ENS_estvide(ensemble e){
  if(e[0] == 0){
    return 1;
  }
  else{
    return 0;
  }
}

/*afficher l'ensemble e sur la sortie standard*/
int ENS_afficher(ensemble e){
  int i=1;
  int card=0; /*compte le nbr d'elements de l'ensemble affiches*/
  
  if(e[0]==0){
    printf("Votre ensemble est vide.\n");
    return 0;
  }
  
  printf("{");
  while(card < e[0]){ /*tant qu'on n'a pas afficher tous les elements*/
    if(e[i] == 1){
      printf(" %d,", i);
      card++;
    }
    i++;
  }
  printf("}\n");
  return 1;
}

/*retourne un element de l'ensemble e. Cet element est egalement retire de l'ensemble. S'il n'y a pas d'element a retirer, la fonction doit retourner -1*/
int ENS_retirer_un_element(ensemble e){
  int card=0;
  int i=0;
  
  if(e[0]==0){ /*cas ou il n'y a pas d'elements*/
    return -1;
  }
  while(card != 1){
    i++;
    if(e[i]==1){
      e[i]=0; /* on le retire de l'ensemble*/
      e[0]--; /*donc on enleve 1 du cardinal*/
      card=1;
    }
  }
  return i;
}

/*ajoute tous les elements de l'ensemble s a l'ensemble d*/
int ENS_ajouter_les_elements_de(ensemble s, ensemble d){
  int i=0;
  int compteur=0; /*compte le nbr d'elements de l'ensemble s ajoute a d*/

  while(compteur<s[0]){
    if(s[i] == 1){
      compteur++;
      if(d[i]==0){/*si i n'est pas dans d on le rajoute*/
      d[i]=1;
      d[0]++;
      }
    }
    i++;
      }
  return 1;
}

/*affecte l'ensemble d a l'ensemble s, cela detruit le precedent contenu de s*/
int ENS_affecter(ensemble s, ensemble d){
  ENS_initialiser(s);
  ENS_ajouter_les_elements_de(d, s);
  return 1;
}

/*calcule l'intersection des ensembles a et b et place le resultat dans l'ensemble r*/
int ENS_intersection(ensemble a, ensemble b, ensemble r){
  int carda=0;
  int cardb=0;
  int i=1;

  while(carda<a[0] && cardb<b[0]){/*si un des deux ensemble est vide on ne rentre pas dans la boucle*/
    
    if(a[i]==1 && b[i]==1){
      carda++;
      cardb++;
      r[0]++;
      r[i]=1;
    }
    if(a[i]==1 && b[i]==0){
      carda++;
    } 
    if(a[i]==0 && b[i]==1){
      cardb++;
    }
    i++;   
  }
  return 1;
}

/*calcule l'union des ensembles a et b et place le resultat dans l'ensemble r*/
int ENS_union(ensemble a, ensemble b, ensemble r){
  int carda=0;
  int cardb=0;
  int i=1;

  while(carda<a[0] || cardb<b[0]){/*tant qu'il reste un element dans un des deux ensembles*/
    if(a[i]==1 || b[i]==1){
      r[0]++;
      r[i]=1;
      if(a[i]==1){
	carda++;
      }
      if(b[i]==1){
	cardb++;
      }
    }
    i++;
  }
  return 1;
}

/*retourne 1 si les ensembles a et b sont egaux, 0 sinon*/
int ENS_egal(ensemble a, ensemble b){
  int carda=0;
  int cardb=0;
  int i=1;
  int egaux=1;
  
  if(a[0]!=b[0]){ /*si pas le meme cardinal ils sont differents*/
    return 0;
  }
   
  while(egaux==1 && carda<a[0] && cardb<b[0]){ 
    if(a[i]!=b[i]){
      egaux=0;
    }
    if(a[i]==1){
      carda++;
    }
    if(b[i]==1){
      cardb++;
    }
    i++;
  }
  if(egaux==1){
    return 1;
  }
  else{
    return 0;
  }
}





