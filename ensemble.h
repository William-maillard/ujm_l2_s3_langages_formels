#define TMAX 100
typedef int ensemble[TMAX];/*on choisi la case d'indice 0 pour stocker le cardinal, et le nombre i est present si la case d'indice i vaut 1, 0 sinon*/

/*declaration des 11 fonctions sur les ensembles*/

/*initialise l'ensemble e*/
int ENS_initialiser(ensemble e);

/*ajoute l’élément elem a l’ensemble e*/
int ENS_ajouter(int elem, ensemble e);

/* retourne 1 si l’élément elem appartient à l’ensemble e; 
0 sinon */		
int ENS_appartient(int elem, ensemble e);

/*retourne 1 si l’ensemble est vide, 0 sinon*/
int ENS_estvide(ensemble e);

/*afficher l'ensemble e sur la sortie standart*/
int ENS_afficher(ensemble e);

/*retourne un element de l'ensemble e. Cet element est egalement retire de l'ensemble. S'il n'y a pas d'element a retirer, 
la fonction doit retourner -1*/
int ENS_retirer_un_element(ensemble e);

/*ajoute tous les elements de l'ensemble s a l'ensemble d*/
int ENS_ajouter_les_elements_de(ensemble s, ensemble d);

/*affecte l'ensemble d a l'ensemble s, cela detruit le precedent
 contenu de s*/
int ENS_affecter(ensemble s, ensemble d);

/*calcule l'intersection des ensembles a et b et place 
le resultat dans l'ensemble r*/
int ENS_intersection(ensemble a, ensemble b, ensemble r);

/*calcule l'union des ensembles a et b et place le resultat 
dans l'ensemble r*/
int ENS_union(ensemble a, ensemble b, ensemble r);

/*retourne 1 si les ensembles a et b sont egaux, 0 sinon*/
int ENS_egal(ensemble a, ensemble b);
