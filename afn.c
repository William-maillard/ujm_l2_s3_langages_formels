#include<stdlib.h>
#include<stdio.h>
#include"ensemble.h"
#include"pile.h"
#define NB_ETATS_MAX 40
#define TAILLE_ALPHABET 2

/*___Fiche n�3___*/

typedef struct afn_t{
  ensemble final;
  ensemble initial;
  ensemble trans[NB_ETATS_MAX][TAILLE_ALPHABET];
}afn_t;

typedef struct afn_t* afn;

/*initialise un AFN avant sa premiere utilisation et retourne l'AFN*/
afn AFN_initialiser(){
  int i, j;
  afn n;

  n= (afn)malloc(sizeof(afn_t));
  ENS_initialiser(n->final);
  ENS_initialiser(n->initial);
  for(i=0; i<NB_ETATS_MAX; i++){
    for(j=0; j<TAILLE_ALPHABET; j++){
      ENS_initialiser(n->trans[i][j]);
    }
  }
  return n;
}

/*rend l'etat e final dans l'afn a*/
int AFN_rendre_final(int e, afn a){
  return ENS_ajouter(e, a->final);
}

/*rend l'etat e initiale dans l'afn a*/
int AFN_rendre_initial(int e, afn a){
  return ENS_ajouter(e, a->initial);/*la fct renvoie un entier*/
}

/*ajoute la transition (p,x,r) dans l'AFN a
tran[i][j] contient l'ensembles des etats q tels que (i, j, q)
*/
int AFN_ajouter_transition(int p,char x, int r, afn a){
  int c=x;
  
  if( c<'a' && c>'z'){
    printf("Parametre invalide. Veuillez rentrer en parametre une lettre de a a z en minuscule.\n");
    return 0;
  }
  c-='a'; /*pour que a=0 et z =25*/
  return ENS_ajouter(r, a->trans[p-1][c]); /*car a l'indice i=0 on a l'etat 1 => decalage de 1 entre indice et etat*/
}

/*affiche l'AFN a*/
int AFN_afficher(afn a){
  int i;
  int j;

  /*affichage de ou des etats initiaux*/
  if(ENS_estvide(a->initial)){ /*vaut 1 si l'ensemble est vide*/
    printf("Il n'y a pas d'etat initial.\n");
  }
  else{
    printf("L'ensemble des etats initiaux est: ");
    ENS_afficher(a->initial);
    printf("\n");
  }

  /*affichage des transitions*/
  for(i=0; i<NB_ETATS_MAX; i++){
    for(j=0; j<TAILLE_ALPHABET; j++){
      if(ENS_estvide(a->trans[i][j]) == 0){ /*si l'ensemble des q tel que (i,j,q) est non vide*/
	printf("L'ensembles des transitions q tel que (%d,%c,q) sont: ", i+1, j+97);
	ENS_afficher(a->trans[i][j]);
	printf("\n");
      }
    }
  }

  /*affichage de l'ensemble de ou des etats finaux*/
  if(ENS_estvide(a->final)){ /*vaut 1 si l'ensemble est vide*/
    printf("Il n'y a pas d'etat final.\n");
  }
  else{
    printf("L'ensemble des etats finaux est: ");
    ENS_afficher(a->final);
    printf("\n");
  }
  return 1;
}


/*___Fiche n�4___*/

/*retourne 1 si le mot est reconnu par l'AFN a, 0 sinon*/
int AFN_analyser_mot(char *mot, afn a){
  ensemble initialBis;/*pour stocker les etats initiaux de a*/
  pile p, p2;
  int etat;
  int i; /*indice pour la chaine de caractere*/
  ensemble ENSb; /*ensemble pour contenir les etats q tesl que (p,mot[i],q) a l'etat i*/
  ensemble inter; /*stocke l'intersection de ENSb avec l'ensembles des etats finaux*/
  int reconnaissable= 0;

  /*On initialise nos ensembles et la pile*/
  PILE_initialiser(p);
  PILE_initialiser(p2);
  ENS_initialiser(ENSb);
  ENS_initialiser(inter);
  ENS_initialiser(initialBis);
  ENS_affecter(initialBis, a->initial);/*c'est pour pouvoir rempiler les etats initiaux sans modifier l'essembles initial de a*/
  
  while( !(ENS_estvide(initialBis)) ){
    /*On retire un element(=1 etat initial) et on le met sur la pile*/
    PILE_empiler(ENS_retirer_un_element(initialBis), p);
  }
  
  /*Ensuite, on test si le mot est reconnaissable a partir de chaque etat initial, tant que aucun chemin reussi n'a ete trouve*/
  while( !(PILE_estvide(p)) && reconnaissable == 0){
     i= 0;
    /*on prend un etat initial*/
     etat= PILE_depiler(p);
    /*On stocke les etats successeurs q tel que (eta,mot[i],q) dans ENSb*/
     ENS_affecter(ENSb, a->trans[etat][(int)mot[i]]);
     i++;
     /*tant que l'on a des etats successeurs, et le mot n'est pas fini*/
     while( !(ENS_estvide(ENSb)) && mot[i]!='\0'){
       /*On empile les elements de ENSb sur la 2 pile*/
       while( !(ENS_estvide(ENSb))){
	 PILE_empiler(ENS_retirer_un_element(ENSb), p2);
       }
       /*On depile chaque element dans etat et s'il existe q tq (etat, mot[i],q) alors on ajoute q dans ENSb*/
       while(!(PILE_estvide(p2))){
	 etat= PILE_depiler(p2);
	 ENS_ajouter_les_elements_de(a->trans[etat][(int)mot[i]], ENSb);/*on stocke tous les etats successeurs dans b*/
       }
       i++; /*on passe a la lettre suivante*/
     }

     if( !(ENS_estvide(ENSb)) && mot[i]=='\0'){/*Si on a lu tous le mot et qu'on a des etat successeur on test si au moins un de ces derniers est final*/
       ENS_initialiser(inter);
       ENS_intersection(ENSb, a->final, inter);
       if( !(ENS_estvide(inter)) ){
	 reconnaissable= 1; /*si il y a un etat final dans b alors le mot est reconaissable*/
       }
     }
     /*Sinon on va refaire un tour de boucle car le mot n'est pas reconnaissable par cet etat initial*/
  }
  return reconnaissable;
}

/*consrtuit l'automate determinise de l'automate a et place le resultat dans r*/
int AFN_determiniser(afn a, afn automate_des_parties){
  ensemble correspondance[NB_ETATS_MAX];
  ensemble successeur[TAILLE_ALPHABET];
  int i=0, etat_traite=0, j, k;/*i indique la case vide, etat_a_traiter l'indice de l'etat a traiter*/
  /* afn automate_des_parties;*/
  pile p;
  ensemble ensembleBis; 
  int etat;
  int transition_ajoute;
  
  /*On initialise nos ensembles et la pile*/
  automate_des_parties= AFN_initialiser();
  PILE_initialiser(p);
  ENS_initialiser(ensembleBis);
  /*On va faire l'automate des parties de a en ne concervant que les etats accessibles, 
    on renomera les parties par un entier et on stockera la correspondance entier/ensemble dans le tableau correspondance.
*/
  /*
    1.On place l'etat initial dans correspondance[i],
    2.On empile les etats de cet ensemble dans p
    3.On depile un etat et on ajoute ses sucesseurs dans le tableau successeur.
    On boucle etape 3 tant que la pile n'est pas vide.
    4.Ensuite on compare le tableau successeur avec le tableau correspondance, et on rajoute les etats qui ne sont pas present dans ce dernier.
    5.On cree les transitions de l'automate_des_parties avec le tableau correspondance.

    Tant qu'il y a des etats a traiter.
*/
  ENS_affecter(correspondance[i], a->initial);
  AFN_rendre_initial(i+1, automate_des_parties); /*l'etat initial est l'etat 1*/
  i++;
  while(etat_traite > i){/*tant qu'il reste un etat a traiter*/
    ENS_affecter(ensembleBis, correspondance[etat_traite]);
    for(k=0; k<TAILLE_ALPHABET; k++){/*on reinitialisse le tableau des successeurs*/
      ENS_initialiser(successeur[k]);
    }
    
    while( !(ENS_estvide(ensembleBis)) ){/*on stocke les etats dans la pile*/
      PILE_empiler(ENS_retirer_un_element(ensembleBis), p);
    }
    while( !(PILE_estvide(p)) ){/*tant que l'on a pas traite tous les elements*/
      etat= PILE_depiler(p);
      
      for(j=0; j<TAILLE_ALPHABET; j++){/*on place les etats successeur dans notre tableau*/
	ENS_ajouter_les_elements_de(a->trans[etat][j],successeur[j]);
      }
    }
    /*On ajoutes nos transition avec la nouvelle correspondance entier/ensemble*/
    for(j=0; j<TAILLE_ALPHABET; j++){
      transition_ajoute= 0;
      while(k < i  &&  transition_ajoute == 0){
	if(ENS_egal(successeur[j], correspondance[k])){
	  AFN_ajouter_transition(etat_traite+1, j, k, automate_des_parties);
	    transition_ajoute=1;
	  }
	  k++;
      }
      if(transition_ajoute == 0){/*il faut rajouter l'ensemble a notre tableau de correspondance*/
	ENS_affecter(correspondance[i], successeur[j]);
	AFN_ajouter_transition(etat_traite+1, j, k, automate_des_parties);
	i++;
      }
    }
    etat_traite++;    
  }

  /*Maintenant, il nous faut definir l'ensemble des etat finaux de l'automate_des_parties:
il faut verifier dans notre tableau correspondance si l'ensemble contient un etat_final, si oui on ajoute l'indice+1 de cet ensemble a l'etat final de l'automate.
  */
  ENS_affecter(ensembleBis, a->final); 
  while( !(ENS_estvide(ensembleBis)) ){/*on stocke les etats finaux dans la pile*/
    PILE_empiler(ENS_retirer_un_element(ensembleBis), p);
  }
  while( !(PILE_estvide(p)) ){/*tant que l'on a pas traite tous les elements*/
    etat= PILE_depiler(p);
    for(k=0; k<i; k++){
      if(ENS_appartient(etat, correspondance[k])){
	AFN_rendre_final(etat, automate_des_parties);
      }
    }
  }

  /*Maintenant, on part des etats finaux, et on place les transitions dans r, cela permet de supprimer les possibles etat non co-accessibles*/
  return 1;
  
  
}
