#include<stdio.h>
#include<stdlib.h>
#define TMAX 100

typedef int pile[TMAX]; /*L'indice 0 stockera le nombre d�l�ment de notre liste et le dernier element insere se trouvera a l'indice i= au nombre d'element de la pile*/

/*initialise la pile p avant sa premiere utilisation*/
int PILE_initialiser(pile p){
  int i;
  
  for(i=0; i<TMAX; i++){
    p[i]=0;
  }
  return 1;
}

/*empile un element sur la pile p*/
int PILE_empiler(int elem, pile p){
  int top=p[0]+1; /*on prends l'indice du dernier element +1 pour se placer en haut de la pile*/
  if(top > TMAX-1){
    printf("Pile pleinne, impossible d'empiler l'element.\n");
    return 0;
  }
  p[top]=elem;
  p[0]++;
  return 1;
}

/*retourne 1 si la pile p est vide 0 sinon*/
int PILE_estvide(pile p){
  if(p[0] == 0){
    return 1;
  }
  else{
    return 0;
  }
}

/*retourne l'element sommet de la pile p et enleve cet element de la pile*/
int PILE_depiler(pile p){
  int sommet;
  int card=p[0];

  if(card == 0){
    printf("Votre pile est vide, il n'y a rien a depiler.\n");
    return 0;
  }
  
  sommet=p[card]; /*on recupere le sommet de la pile*/
  p[card]=0;
  p[0]--; /*on enleve un element a la pile*/
  return sommet;
}

/*affiche la pile p*/
int PILE_afficher(pile p){
  int card= p[0];
  int i;
  
  if(card == 0){
    printf("Votre pile est vide.\n");
    return 0;
  }
  
  printf("__HAUT__\n");
  for(i=0; i<card; i++){
    printf("%d\n", p[card-i]); /*le haut de la pile se trouve a la fin de notre liste*/
  }
  printf("__BAS__\n");
  return 1;
}

